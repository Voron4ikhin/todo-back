<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'title', 'content',
        'tag_id', 'deadline', 'active',
    ];

    protected $dates = [
        'deadline',
    ];

    public static $rules = [
        'user_id' => ['required', 'integer'],
        'title' => ['required', 'string', 'max:100'],
        'content' => ['required', 'string', 'max:10000'],
        'deadline' => ['nullable', 'string', 'date'],
        'active' => ['nullable', 'boolean'],
        'tag_id' => ['required', 'integer'],
    ];

}
