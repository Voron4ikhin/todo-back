<?php

namespace App\Http\Controllers\User;

use App\Models\Task;
use App\Models\User;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class TaskController extends Controller
{
    public function index(Request $request)
    {
        $validated = $request->validate([
            'deadline' => ['nullable', 'string'],
            'active' => ['nullable', 'integer'],
            'created' => ['nullable', 'string'],
            'tag' => ['nullable', 'string'],
        ]);

        $query = Task::query();

        $active = $validated['active'] ?? null;
        if($active !== null) {
            $query->where('active', $active);
        }

        if($validated['deadline'] ?? null){
            $query->orderBy('deadline', $validated['deadline']);
        }

        if($validated['created'] ?? null){
            $query->orderBy('created_at', $validated['created']);
        }

        if($validated['tag'] ?? null){
            $query->orderBy('tag_id', $validated['tag']);
        }

        return $query->paginate(12);
    }

    public function store(Request $request)
    {
        $validated = $request->validate(Task::$rules);
        return Task::query()->create($validated);
    }

    public function show(Request $request, $task)
    {
        return Task::query()->find($task);
        //с использованием кэша
//        return cache()->remember(
//            key: "task.{$task}",
//            ttl: now()->addHour(),
//            callback: function () use($task){
//                return Task::query()->find($task);
//        });
    }

    public function update(Request $request, $task)
    {
        $validated = $request->validate([
            'title' => ['string', 'max:100'],
            'content' => ['nullable', 'string', 'max:10000'],
            'deadline' => ['nullable', 'string', 'date'],
            'active' => ['nullable', 'boolean'],
            'tag_id' => ['nullable', 'integer'],
        ]);
        $task = Task::query()->find($task);
        if($task){
            return $task->update($validated);
        }
        return 'Task not existed';
    }

    public function delete($task)
    {
        $task = Task::query()->find($task);
        if($task){
            return $task->delete();
        }
        return 'Already deleted';
    }
}
