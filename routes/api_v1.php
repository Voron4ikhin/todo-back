<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\User\TaskController;

Route::get('/', function(){
    return [];
});


//регистрация нового пользователя в бд
Route::post('register', [RegisterController::class, 'store'])->name('register.store');

Route::prefix('user')->group(function () {
    Route::get('tasks', [TaskController::class, 'index'])->name('user.tasks'); //получение всех задач пользователя
    Route::post('tasks', [TaskController::class, 'store'])->name('user.tasks.store'); //создание задачи
    Route::get('tasks/{task}', [TaskController::class, 'show'])->name('user.tasks.show')->whereNumber('task'); //детальная страница таски по id
    Route::patch('tasks/{task}', [TaskController::class, 'update'])->name('user.tasks.update')->whereNumber('task'); //изменение задачи
    Route::delete('tasks/{tasks}', [TaskController::class, 'delete'])->name('user.tasks.delete')->whereNumber('task');  //удаление задачи
});

